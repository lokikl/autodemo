#!/usr/bin/env ruby

command = ARGV[0]

require 'faye'
require 'permessage_deflate'

port     = 58080
path     = 'stream'
scheme   = 'https'
endpoint = "#{scheme}://localhost:#{port}/#{path}"
proxy    = {:headers => {'User-Agent' => 'Faye'}}

EM.run {
  client = Faye::Client.new(endpoint, :proxy => proxy)
  client.add_websocket_extension(PermessageDeflate)

  timer = EM::Timer.new(2, nil) {
    puts "timeout"
    client.disconnect
    EM.stop
  }

  subscription = client.subscribe('/bar') do |message|
    puts message['output']
    subscription.unsubscribe
    client.disconnect
    EM.stop
  end

  subscription.callback do
    publication = client.publish("/foo", {
      javascript: command,
    })
    publication.errback do |error|
      puts "[PUBLISH FAILED] #{error.inspect}"
    end
  end

  subscription.errback do |error|
    puts "[SUBSCRIBE FAILED] #{error.inspect}"
  end
}
