#!/usr/bin/env ruby

# puma -p 55555 -C "-" capture.ru

`mkdir -p output`

def get_chrome_geometry
  wmctrl_output = `wmctrl -lG | grep 'Chrome'`
  _, _, x, y, w, h, *_ = wmctrl_output.split(' ')
  [x.to_i, y.to_i, w.to_i, h.to_i]
end

def capture(id)
  x, y, w, h = get_chrome_geometry
  puts "start capturing"
  system('wmctrl -a "Chrome"')
  cmd = %Q[byzanz-record --exec="./capture-server/loop.sh" --x=#{x} --y=#{y} --width=#{w} --height=#{h} #{id}.gif]
  pid = spawn(cmd)
  Process.detach(pid)
end

run Proc.new { |env|
  path = env["REQUEST_PATH"]
  if path.start_with?('/start/')
    id = path.split('/', 3)[-1]
    capture(id)
    ['200', {}, ['started']]
  elsif path.start_with?('/stop')
    pid = File.read('loop.pid').strip rescue nil
    if pid
      system(%Q[kill #{pid}])
      ['200', {}, ['stopped']]
    else
      ['200', {}, ['no session found']]
    end
  else
    ['200', {}, ['invalid path']]
  end
}
