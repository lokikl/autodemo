#!/usr/bin/env bash
set -e

echo $$ > loop.pid

# SIGTERM-handler
term_handler() {
  echo 'SIGTERM received'
  rm loop.pid
  exit 0; # 128 + 15 -- SIGTERM
}
trap 'term_handler' SIGTERM

while true; do
  tail -f /dev/null & wait ${!}
done
