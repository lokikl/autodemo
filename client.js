var __inject_js = function(d, test, s, src, onload) {
  if (test === "undefined") {
    var js, fjs = d.getElementsByTagName(s)[0];
    js = d.createElement(s);
    js.onload = function(){ onload() };
    js.src = src;
    fjs.parentNode.insertBefore(js, fjs);
  } else {
    onload()
  }
};
__inject_js(document, typeof(jQuery), 'script', 'https://code.jquery.com/jquery-1.12.4.min.js', function() {
  __inject_js(document, typeof(Faye), 'script', 'https://localhost:58080/stream/client.js', function() {
    var client = new Faye.Client('https://localhost:58080/stream');

    var subscription = client.subscribe('/foo', function(message) {
      console.log('received command', message.javascript)
      var output = eval(message.javascript)
      client.publish('/bar', {
        output: output
      })
    });

    console.log("autodemo client started")
  })
})
